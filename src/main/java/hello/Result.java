package hello;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {

    private int id;
	private String nid;
	private String token;

    public Result() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getNid() {
        return nid;
    }

    public void setId(String nid) {
        this.nid = nid;
    }
	
	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id='" + id +
                ", nid=" + nid +
				", token=" + token +
                '}';
    }
}