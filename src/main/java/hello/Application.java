package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;


public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String args[]) {
        RestTemplate restTemplate = new RestTemplate();
        Result res = restTemplate.postForObject("http://vir.inf.u-szeged.hu:8090/getToken?nid=U8AYLC", null, Result.class);
        log.info(res.toString());
		
		CouchDbClient dbClient = new CouchDbClient(
			"vir-db",
			false,
			"http",
			"vir.inf.u-szeged.hu",
			5986,
			"dbreader",
			"ReadOnlyUser");
		Response response = dbClient.save(res);
		log.info(response.getId());
		dbClient.shutdown();
    }

}